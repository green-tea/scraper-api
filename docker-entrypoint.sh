#!/bin/bash
db=$1
port=$2

# check whether db is up
while !</dev/tcp/$db/$port; do 
  sleep 1 
done

# nc and curl caused roughly a minute of delay before starting the server even though db was ready

# while ! nc $db $port; do
#   sleep 1
# done

sh ./build.sh &
python manage.py migrate
python manage.py runserver 0.0.0.0:8080

