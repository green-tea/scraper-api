import requests, time, json
from scraper.scraper import Scraper


scraper = Scraper()
articles = scraper.scrap_async()

# wait for server to be ready
response = requests.get('http://localhost:8080/stats/')
while response.status_code is not 200:
    time.sleep(1)
    requests.get('http://localhost:8080/stats/')

headers = {'Content-Type': 'application/json', 'Accept':'application/json'}

# scrap only if there is no data
if not response.json():
    requests.post('http://localhost:8080/scrap/', data=json.dumps(articles), headers=headers)   
    print('Scraped and saved') 
