from django.db import models

# class Author(models.Model):
#     nickname = models.CharField(max_length=60, unique=True)
#     name = models.CharField(max_length=30)
#     surname = models.CharField(max_length=30) 

class Article(models.Model):
    title = models.CharField(max_length=150)
    # author = models.ForeignKey(Author, max_length=60, blank=False)
    author = models.CharField(max_length=60, blank=False)
    author_original = models.CharField(max_length=60, default=None, null=True)
    content = models.TextField()

    # methods to save here
    # would need to initialize Article in views..
    def save_articles(self, articles):
        for article in articles:
            self.objects.create(article)
