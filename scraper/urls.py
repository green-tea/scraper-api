from django.urls import path

from . import views

urlpatterns = [
    path('', views.ScraperView.as_view())
]