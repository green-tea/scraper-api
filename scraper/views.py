from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Article
from .scraper import Scraper


class ScraperView(APIView):

    def get(self, request): 
        scraper = Scraper()
        articles = scraper.scrap_async()

        Article.objects.all().delete()

        for article in articles:
            Article.objects.create(**article)

        return Response(articles)

    def post(self, request):
        articles = request.data

        if not articles:
            return Response('Could not save articles', status=status.HTTP_404_NOT_FOUND) 

        Article.objects.all().delete()

        for article in articles:
            Article.objects.create(**article)

        return Response('Articles have been saved')
