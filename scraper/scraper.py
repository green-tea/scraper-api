import requests, re, asyncio, aiohttp
from bs4 import BeautifulSoup
from .special_characters import special_characters


class Scraper():

    url = 'https://teonite.com'

    def __init__(self):

        source = requests.get(f'{self.url}/blog').text
        self.soup = BeautifulSoup(source, 'lxml')

    # 4551 ms, usually 6-7 seconds
    # synchronously ~20s - removed
    def scrap_async(self):

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        articles = loop.run_until_complete(self.pages_async())
        loop.close()

        # returned value is an array of arrays
        # each inner array consists of aritcles for given page
        flattened_articles = []
        for inner_articles in articles:
            flattened_articles = [*flattened_articles, *inner_articles]
        return flattened_articles

    async def pages_async(self):
        # Based on page count: 'Page 1 of 4'
        previous_page = self.soup.find('nav', class_='pagination').find('a', class_='older-posts')
        # remove page number from href: /blog/page/2/ into /blog/page/
        previous_page_href = previous_page.get('href')[:-2]        
        page_count = self.soup.find('nav', class_='pagination').span.text.strip()
        total_page_count = int(page_count[-1])

        # Create a list of all the pages hrefs
        urls = []
        for page in range(2, total_page_count + 1):
            urls.append(f'{self.url}{previous_page_href}{page}')

        async with aiohttp.ClientSession() as session:
            tasks = [self.page()]
            for url in urls:
                tasks.append(self.page_async(url, session))   
            return await asyncio.gather(*tasks)
                    


    async def page_async(self, page, session):

        async with session.get(page) as response:
            entire_page = await response.text()
            soup = BeautifulSoup(entire_page, 'lxml')
            urls = []

            for article in soup.find_all('article'):
                article_url_more = article.find('a', class_='read-more').get('href')
                article_url = f'{self.url}{article_url_more}'
                urls.append(article_url) 

            async with aiohttp.ClientSession() as session:
                tasks = []
                for url in urls:
                    tasks.append(self.article(url, session)) 

                return await asyncio.gather(*tasks)
            


    def scrap(self):
        # Scraps as long as there is a link to older posts
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        articles = loop.run_until_complete(self.page())

        previous_page = self.soup.find('nav', class_='pagination').find('a', class_='older-posts')

        while previous_page:
            previous_page_url = previous_page.get('href')
            source = requests.get(f'{self.url}{previous_page_url}').text
            self.soup = BeautifulSoup(source, 'lxml')

            articles_new = loop.run_until_complete(self.page())
            articles = [*articles, *articles_new]
            previous_page = self.soup.find('nav', class_='pagination').find('a', class_='older-posts')

        loop.close()
        return articles

    async def page(self):

        urls = []
        for article in self.soup.find_all('article'):
            article_url_more = article.find('a', class_='read-more').get('href')
            article_url = f'{self.url}{article_url_more}'
            urls.append(article_url)

        async with aiohttp.ClientSession() as session:
            tasks = []
            for url in urls:
                tasks.append(self.article(url, session))   

            return await asyncio.gather(*tasks)

    async def article(self, article, session):

        article_dict = {}
        async with session.get(article) as response:
            entire_article = await response.text()
            soup = BeautifulSoup(entire_article, 'lxml')
            title = soup.article.header.find('h1', class_='post-title').text
            content = soup.article.find('section', class_='post-content').stripped_strings

            content_string = ''
            for part in content:
                # Add a space before each part
                content_string += f' {part}'
                # remove space before . and ,
                # strip to remove spaces from beginning and end, possibly move it to just strip entire content
                # stripped_string doesn't remove all \n so replace them with a space
                content_string = re.sub('\s[.,]', '.', content_string).strip().replace('\n', ' ')

            author = soup.footer.find('span', class_='author-content').h4.text
            author_nickname = author.lower().replace(' ','')
            author_original = author_nickname

            for character in author_nickname:
                if character in special_characters:
                    author_nickname = author_nickname.replace(character, special_characters[character])
                    
            article_dict['title'] = title
            article_dict['author'] = author_nickname
            article_dict['content'] = content_string

            if author_original is not author_nickname:
                article_dict['author_original'] = author_original

            return article_dict
