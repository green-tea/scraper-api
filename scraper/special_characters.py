special_characters = {
    'ł': 'l',
    'ą': 'a',
    'ń': 'n',
    'ę': 'ę',
    'ó': 'o',
    'ź': 'z',
    'ż': 'z',
    'ć': 'c',
    'ś': 's'
}