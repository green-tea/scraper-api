FROM python:3.6

ENV PYTHONUNBUFFERED 1

# RUN apt-get install curl -y
# RUN apt-get update && apt-get install netcat-openbsd -y 

RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install -r requirements.txt

# COPY ./docker-entrypoint.sh /
# ENTRYPOINT [ "/docker-entrypoint.sh" ]