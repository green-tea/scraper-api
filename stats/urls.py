from django.urls import path

from . import views

urlpatterns = [
    path('', views.StatsView.as_view()),
    path('<author>', views.StatsAuthorView.as_view()) 
]