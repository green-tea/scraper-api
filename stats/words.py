import re, operator

def get_words(articles, n=10):
        wordsDictionary = {}
        for article in articles:
            content = article['content'].split()
            for word in content:
                # remove any list item that is not alphanumeric
                # remove numbers
                if word.isalnum() and not(word.isdigit()):
                    no_special_characters = re.sub('\W+', '', word).lower()
                    if no_special_characters in wordsDictionary:
                        wordsDictionary[no_special_characters] += 1
                    else: 
                        wordsDictionary[no_special_characters] = 1        
            
        selectedWords = {}
        sortedWords = sorted(wordsDictionary.items(), key=lambda key: key[1], reverse=True)[:n]
        for item in sortedWords:
            selectedWords[item[0]] = item[1]
        
        return selectedWords