from django.db.models import Q

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from scraper.models import Article
from scraper.serializers import ArticleSerializer

from .words import get_words


class StatsView(APIView):
    
    def get(self, request):
        articles = Article.objects.all()
        serialized_articles = ArticleSerializer(articles, many=True)

        return Response(get_words(serialized_articles.data))


class StatsAuthorView(APIView):

    def get(self, request, author):
        author_articles = Article.objects.filter(Q(author=author) | Q(author_original=author))

        if not author_articles:
            return Response('Author not found', status=status.HTTP_404_NOT_FOUND)

        serialized_articles = ArticleSerializer(author_articles, many=True)

        return Response(get_words(serialized_articles.data))


